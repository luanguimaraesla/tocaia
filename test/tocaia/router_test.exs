defmodule Tocaia.RouterTest do
  use ExUnit.Case
  use Plug.Test

  alias Tocaia.Router

  @command "ls"
  @arguments "-l -a"

  @opts Router.init([])

  test "returns 201" do
    conn = conn(:post, "/execute", "command=pwd")
    |> put_req_header("content-type", "application/x-www-form-urlencoded")
    |> Router.call(@opts)

    assert conn.state == :sent
    assert conn.status == 201
  end

  test "returns 404" do
    conn = conn(:get, "/missing", "")
    |> Router.call(@opts)

    assert conn.state == :sent
    assert conn.status == 404
  end
end
