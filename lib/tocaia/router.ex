defmodule Tocaia.Router do
  use Plug.Router

  alias Tocaia.Plug.VerifyRequest
  alias Tocaia.Executor

  plug Plug.Parsers, parsers: [:urlencoded, :multipart]
  plug VerifyRequest, fields: ["command"],
                      paths:  ["/execute"]

  plug :match
  plug :dispatch

  post "/execute", do: Executor.execute(conn)
  match _, do: send_resp(conn, 404, "Oops!")
end
