defmodule Tocaia do
  @moduledoc """
  Documentation for Tocaia.
  """

  @doc """
    Tocaia is a beatiful handler for DevOps work
  """

  use Application
  require Logger

  def start(_type, _args) do
    port = Application.get_env(:tocaia, :cowboy_port, 3000)
  
    children = [
      Plug.Adapters.Cowboy.child_spec(
        :http,
        Tocaia.Router,
        [],
        port: port
      )
    ]
    Logger.info "Started application"
    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
