defmodule Tocaia.Watcher do
  alias Tocaia.Executor

  def info do
    %{
      user: user(),
      date: date(),
      pwd: pwd()
    }
  end

  defp user do
    #[TODO] implement arguments
    #Executor.execute_command("echo $USER")
    "undefined"
  end

  defp date do
    Executor.execute_command("date")
  end

  defp pwd do
    Executor.execute_command("pwd")
  end
end
