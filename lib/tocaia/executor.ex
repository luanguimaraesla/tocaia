defmodule Tocaia.Executor do
  import Plug.Conn
  alias Tocaia.Watcher

  def execute(conn) do
    arguments = extract_arguments(conn)

    conn
    |> extract_command
    |> execute_command(arguments)
    |> create_json
    |> answer(conn)
  end

  def execute_command(command, arguments \\ []) do
    case System.cmd(command, arguments) do
      {response, 0} ->
        response
      { _ , err } ->
        "Error: Nº#{err}"
    end
  end

  defp extract_command(conn) do
    if Map.has_key?(conn.params, "command") do
      conn.params["command"]
    else
      raise ArgumentError, message: "There is no command"
    end
  end

  defp extract_arguments(conn) do
    if Map.has_key?(conn.params, "arguments") do
      conn.params["arguments"]
      |> String.split
    else
      [] 
    end
  end

  defp answer(json, conn) do
    conn
    |> put_resp_content_type("application/json") # JSON type
    |> send_resp(201, json) # send it!
  end

  defp create_json(response) do
    json = %{
      response: response,
      info: Watcher.info()
    } |> Poison.encode! 
    json
  end
end
