FROM "elixir"

EXPOSE 3000

ADD . /tocaia
WORKDIR /tocaia

RUN mix local.hex --force; \
    mix deps.get;

CMD mix run --no-halt;\
    sleep infinity;
